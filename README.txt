Image Circle Slider

Table of Contents:
==================
1. Intorduction
2. Requirements
3. Installation
4. Maintainers


Introduction
============
Image CircleSlider provides the user the flexibility to present their images
in a more attractive and modern way in front of the users.
It just modifies the views of the images in place of a list to a circular
slider which the user can slide upon to have a nice view of the images.
The slider can be used with as many images one likes but gives the best
result when the picture size is greater or equal to 350pixels * 350 pixels.

============
This module requires the views module.

Installation
============

Download the js and css of the jquery plugin from
http://baijs.com/tinycircleslider/.
Create a new folder in sites/all/libraries named imagecirclesliderlib and keep
the js, css and images in the folder.

Now follow these steps to use the Image CircleSlider:
1. Download and install the module
2. Create a view for your images.
3. Go on the format option and select Image CircleSlider and save it.
4. Now you can view ur images in the form of circleslider

This module can also be used for field formatter

Follow these steps:
1. Create a content type for your images
2. Go to Structure->Content Types->(Your Content Type)->Manage Display.
3. In the Format Option, select Image Circle Slider and save it.
4. You have done it..Now see the node, it has Slider for your images


Maintainers
===========
Arpit Jalan: https://www.drupal.org/u/ajalan065
[Note: Images should have minimum dimensions of 350 pixelsx350 pixels.]
